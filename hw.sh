#!/bin/bash  
# BIOS-NUMA模式==打开  
if grep -i numa /var/log/dmesg | grep -q "NUMA";then
    echo -e "\033[42mNUMA模式开启\033[0m"
else
    echo -e "\033[41mNUMA模式未开启\033[0m"
fi


#BIOS-CPU模式==performance
if grep -q "performance" /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor; then
    echo -e "\033[42mcpu性能模式\033[0m"
else
    echo -e "\033[41mcpu不是性能模式\033[0m"
fi


CPUnumber=$(cat /proc/cpuinfo | grep "physical id" | sort | uniq | wc -l)
Logical=$(grep "cpu cores" /proc/cpuinfo | uniq | awk '{print $4}')
Totalnumber=$(cat /proc/cpuinfo | grep "processor" |wc -l )
Totalnumber1=$((2*CPUnumber*Logical))
if [ "$Totalnumber1" -eq "$Totalnumber" ];then
    echo -e "\033[42m检测通过【HT开启】\033[0m"
else
    echo -e "\033[41m检测不通过【HT未开启】\033[0m"
    echo "物理cpu数：$a cpu逻辑核数：$b cpu线程总数为：$c"
fi



#cpu频率
cpufrequency=$(cat /proc/cpuinfo|grep -i mhz|awk -F: '{sum += $2} END { print sum/NR }' | xargs printf "%.0f")
if [ "$cpufrequency" -gt 2000 ];then
    echo -e "\033[42mcpu频率检测通过\033[0m"
else
    echo -e "\033[41m频率检测不通过，cpu频率为：$cpufrequency\033[0m"
fi

#冷门阵列卡
if lspci | grep -Eo "RAID bus controller.*" | awk -F: '{print$2}';then
    echo -e "\033[42m冷门阵列卡检测通过\033[0m"
else
    echo -e "\033[41mN冷门阵列卡检测不通过\033[0m"
fi

#磁盘数量
sum=$(lsblk -d -n | grep -c '^sd')
if [ "$sum" -gt 10 ]; then
    echo -e "\033[42m总磁盘数量超过10\033[0m"
else
    echo -e "\033[41m总磁盘数量不超过10\033[0m"
    echo "总磁盘数量为:$sum"
fi



#SSD
ssd_1tb_count=$(lsblk -d -n -o SIZE,ROTA | awk '$2 ~ /0/ && ($1>1 &&$1<10) {count++} END {print count}')
ssd_less1tb_count=$(lsblk -b -d -n | awk '$2 ~ /0/ && ($1>100 && $1 < 1024 {count++} END {print count}' | xargs printf "%.0f")
if [ "$ssd_1tb_count" -gt 1 ]; then
    echo -e "\033[42mSSD(1T)数量大于1\033[0m"
elif [ "$ssd_less1tb_count" -gt 2 ]; then
    echo -e "\033[42mSSD(<1T)数量大于2\033[0m"
else
    echo -e "\033[41mSSD数量条件不满足\033[0m"
    echo "SSD(1T)为：$ssd_1tb_count  SSD(<1T)为：$ssd_less1tb_count"
fi

#磁盘-HDD数量 >10
hdd_names=($(lsblk -d -n -p -o NAME,ROTA | awk '$2==1 {print $1}'))
hdd_count=${#hdd_names[@]}
if [ "$hdd_count" -gt 10 ]; then
    echo -e "\033[42mHDD数量大于10\033[0m"
else
    echo -e "\033[41mHDD数量小于10\033[0m"
        echo "HDD数量为：$hdd_count"
fi
# 获取系统盘的信息，假设系统盘是根目录 /
disk_size=$(df / | awk 'NR==2 {print $2}')

# 检查系统盘是否大于300GB
if [ "$disk_size" -gt 314572800 ]; then
    echo -e "\033[42m系统盘检测通过\033[0m"
else
    echo -e "\033[41m系统盘不足300GB\033[0m"
fi

#!/bin/bash

# 获取总内存大小（以 KB 为单位）
total_memory_kb=$(grep MemTotal /proc/meminfo | awk '{print $2}')

# 将内存大小从 KB 转换为 GB（1GB = 1024*1024 KB）
total_memory_gb=$((total_memory_kb / 1024 / 1024))
total_menory_gb2=$(total_memory_gb | xargs printf "%.0f")

# 检查内存是否大于或等于 64GB
if [ "$total_memory_gb" -ge 64 ]; then
    echo "服务器内存大小：${total_memory_gb} GB，满足大于等于 64 GB 的条件。"
else
    echo "服务器内存大小：${total_memory_gb} GB，不满足大于等于 64 GB 的条件。"
fi




#检查日志错误
dmesg -t | egrep -i 'MEMORY ERROR | Memory read error | Hardware Error'
if dmesg -t | egrep -i 'MEMORY ERROR | Memory read error | Hardware Error';then
    echo -e "\033[41m存在以下错误\033[0m"
	dmesg -t | egrep 'MEMORY ERROR | Memory read error | Hardware Error'
else
    echo -e "\033[42m没有日志错误，检测通过\033[0m"
fi


#内存-插槽是否插对检查==正确
memory_fail=0
memory_names=($(dmidecode|grep -P -A8 "Memory\s+Device" |grep "Locator" | awk '!/NO/ {print $2}'))
for i in {0..7};do
    if ["${#memory_names[i]}" -ne "${#memory_names[((i+7))]}"];then
	    echo -e "\033[41m以下内存槽位不对应\033[0m"
		echo "${memory_names[i]}和${memory_names[((i+7))]}"
		((memory_fail++))
	fi
done
if ["$memory_fail" -eq 0];then
    echo -e "\033[42m内存槽位检测合格\033[0m"
fi

#万兆卡数量检测
ether=$(lspci | grep -i "ethernet" | grep -i "10-G" | wc -l)
if ["$ether" -gt 0];then
    echo -e "\033[42m存在万兆卡，数量为：$ether\033[0m"
else
    echo -e "\033[41m没有万兆卡\033[0m"
fi


# 检查运行的网络接口数量
running_interfaces=$(ip link show | grep -E 'UP' | wc -l)
if [ "$running_interfaces" -ge 1 ]; then
    echo -e "\033[42m正在运行的网卡数量: $running_interfaces\033[0m"
else
    echo -e "\033[41m没有运行中的网卡\033[0m"
fi

#运行中网卡型号
network_cards=(
    "Intel Corporation 82599ES 10-Gigabit SFI/SFP+ Network Connection (rev 01)"
    "Intel Corporation Ethernet Controller X710 for 10GbE SFP+ (rev 02)"
    "Broadcom Inc. and subsidiaries NetXtreme BCM5720 2-port Gigabit Ethernet PCIe"
    "Intel Corporation Ethernet Connection X722 for 10GbE SFP+ (rev 09)"
    "Intel Corporation Ethernet Connection X722 for 1GbE (rev 09)"
    "Intel Corporation Ethernet 10G 2P X520 Adapter (rev 01)"
    "Intel Corporation 82599 10 Gigabit Network Connection (rev 01)"
    "Intel Corporation Ethernet Controller 10-Gigabit X540-AT2 (rev 03)"
    "Mellanox Technologies MT27710 Family [ConnectX-4 Lx]"
    "Intel Corporation 82576 Gigabit Network Connection (rev 01)"
    "Intel Corporation Ethernet Controller X710 for 10GbE SFP+ (rev 01)"
    "Broadcom Inc. and subsidiaries NetXtreme II BCM57800 1/10 Gigabit Ethernet (rev 10)"
    "Broadcom Inc. and subsidiaries NetXtreme BCM5719 Gigabit Ethernet PCIe (rev 01)"
    "Intel Corporation Ethernet Controller 10-Gigabit X540-AT2 (rev 01)"
    "Broadcom Inc. and subsidiaries NetXtreme II BCM5716 Gigabit Ethernet (rev 20)"
    "Broadcom Inc. and subsidiaries BCM57416 NetXtreme-E Dual-Media 10G RDMA Ethernet Controller (rev 01)"
    "Intel Corporation 82580 Gigabit Network Connection (rev 01)"
)
network_count=0
#network_cardss=$(lspci | grep -i "ethernet" | awk '{for(i=4;i<=NF;i++) printf "%s ",$i;printf ","}')
#IFS=',' read -ra network_card <<< "$network_cardss"
network_card=($(lspci | grep -i "ethernet"))
for card in "${network_card[@]}"; do
    for cards in "${network_cards[@]}"; do
	    #if [["$card" == "$cards"]];then
		if [echo "$card" | grep "$cards"];then
		    ((network_count++))
		fi
	done
	if ["$network_count" -eq 0];then
	    echo -e "\033[41m网卡：$card 不是常用网卡\033[0m"
	else
	    echo -e "\033[42m网卡：$card 是常用网卡\033[0m"
	fi
	network_count=0
done

#检测cpu型号是否常用
cpu_cards=("AMD EPYC 3251" "AMD EPYC 7232P" "AMD EPYC 7251" "AMD EPYC 7252" "AMD EPYC 7262" "AMD EPYC 7272"
"AMD EPYC 7281" "AMD EPYC 7282" "AMD EPYC 72F3" "AMD EPYC 7301" "AMD EPYC 7302" "AMD EPYC 7302P" "AMD EPYC 7313"
"AMD EPYC 7313P" "AMD EPYC 7343" "AMD EPYC 7351" "AMD EPYC 7351P" "AMD EPYC 7352" "AMD EPYC 7371" "AMD EPYC 73F3"
"AMD EPYC 7401P" "AMD EPYC 7402" "AMD EPYC 7402P" "AMD EPYC 7413" "AMD EPYC 7443" "AMD EPYC 7443P" "AMD EPYC 7451"
"AMD EPYC 7452" "AMD EPYC 7453" "AMD EPYC 74F3" "AMD EPYC 7501" "AMD EPYC 7502" "AMD EPYC 7502P" "AMD EPYC 7513"
"AMD EPYC 7532" "AMD EPYC 7542" "AMD EPYC 7543" "AMD EPYC 7543P" "AMD EPYC 7551" "AMD EPYC 7551P" "AMD EPYC 7571"
"AMD EPYC 75F3" "AMD EPYC 7601" "AMD EPYC 7642" "AMD EPYC 7643" "AMD EPYC 7662" "AMD EPYC 7702" "AMD EPYC 7702P"
"AMD EPYC 7713" "AMD EPYC 7713P" "AMD EPYC 7742" "AMD EPYC 7763" "AMD EPYC 7773X" "AMD EPYC 7B13" "AMD EPYC 7D12"
"AMD EPYC 7F32" "AMD EPYC 7F52" "AMD EPYC 7J13" "AMD EPYC 7K62" "AMD EPYC 7R32" "AMD EPYC 7T83" "AMD EPYC 7V13"
"AMD EPYC 9354P" "AMD EPYC 9654" "AMD EPYC Embedded 7292P" "AMD Ryzen Embedded V3C48" "Intel Xeon CPU E5-2620 v4"
"Intel Xeon CPU E5-2630 v3" "Intel Xeon CPU E5-2630 v4" "Intel Xeon CPU E5-2650 v3" "Intel Xeon CPU E5-2650 v4"
"Intel Xeon CPU E5-2660 v4" "Intel Xeon CPU E5-2670 v3" "Intel Xeon CPU E5-2673 v3" "Intel Xeon CPU E5-2678 v3"
"Intel Xeon CPU E5-2680 v3" "Intel Xeon CPU E5-2680 v4" "Intel Xeon CPU E5-2682 v4" "Intel Xeon CPU E5-2683 v4"
"Intel Xeon CPU E5-2690 v3" "Intel Xeon CPU E5-2696 v3" "Intel Xeon CPU E5-2698B v3" "Intel Xeon E5-1650 v3"
"Intel Xeon E5-1650 v4" "Intel Xeon E5-1660 v3" "Intel Xeon E5-1660 v4" "Intel Xeon E5-1680 v3" "Intel Xeon E5-1680 v4"
"Intel Xeon E5-1681 v3" "Intel Xeon E5-2618L v3" "Intel Xeon E5-2618L v4" "Intel Xeon E5-2620 v4" "Intel Xeon E5-2628L v3"
"Intel Xeon E5-2628L v4" "Intel Xeon E5-2630 v3" "Intel Xeon E5-2630 v4" "Intel Xeon E5-2630L v4" "Intel Xeon E5-2640 v3"
"Intel Xeon E5-2640 v4" "Intel Xeon E5-2643 v3" "Intel Xeon E5-2643 v4" "Intel Xeon E5-2648L v3" "Intel Xeon E5-2648L v4"
"Intel Xeon E5-2649 v3" "Intel Xeon E5-2650 v3" "Intel Xeon E5-2650 v4" "Intel Xeon E5-2650L v3" "Intel Xeon E5-2650L v4"
"Intel Xeon E5-2658 v3" "Intel Xeon E5-2658 v4" "Intel Xeon E5-2658A v3" "Intel Xeon E5-2660 v3" "Intel Xeon E5-2660 v4"
"Intel Xeon E5-2663 v3" "Intel Xeon E5-2666 v3" "Intel Xeon E5-2667 v3" "Intel Xeon E5-2667 v4" "Intel Xeon E5-2669 v3"
"Intel Xeon E5-2670 v3" "Intel Xeon E5-2673 v3" "Intel Xeon E5-2673 v4" "Intel Xeon E5-2675 v3" "Intel Xeon E5-2676 v3"
"Intel Xeon E5-2676 v4" "Intel Xeon E5-2678 v3" "Intel Xeon E5-2679 v4" "Intel Xeon E5-2680 v3" "Intel Xeon E5-2680 v4"
"Intel Xeon E5-2680R v4" "Intel Xeon E5-2682 v4" "Intel Xeon E5-2683 v3" "Intel Xeon E5-2683 v4" "Intel Xeon E5-2685 v3"
"Intel Xeon E5-2686 v3" "Intel Xeon E5-2686 v4" "Intel Xeon E5-2687W v3" "Intel Xeon E5-2687W v4" "Intel Xeon E5-2689 v4"
"Intel Xeon E5-2690 v3" "Intel Xeon E5-2690 v4" "Intel Xeon E5-2695 v3" "Intel Xeon E5-2695 v4" "Intel Xeon E5-2696 v3"
"Intel Xeon E5-2696 v4" "Intel Xeon E5-2697 v3" "Intel Xeon E5-2697 v4" "Intel Xeon E5-2697A v4" "Intel Xeon E5-2697R v4"
"Intel Xeon E5-2698 v3" "Intel Xeon E5-2698 v4" "Intel Xeon E5-2698B v3" "Intel Xeon E5-2698R v4" "Intel Xeon E5-2699 v3"
"Intel Xeon E5-2699 v4" "Intel Xeon E5-2699A v4" "Intel Xeon E5-2699C v4" "Intel Xeon E5-4620 v3" "Intel Xeon E5-4627 v3"
"Intel Xeon E5-4627 v4" "Intel Xeon E5-4640 v3" "Intel Xeon E5-4650 v3" "Intel Xeon E5-4655 v3" "Intel Xeon E5-4660 v3"
"Intel Xeon E5-4660 v4" "Intel Xeon E5-4667 v3" "Intel Xeon E5-4669 v3" "Intel Xeon E5-4669 v4" "Intel Xeon E7-8880 v3"
"Intel Xeon E7-8891 v3" "Intel Xeon Gold 5115" "Intel Xeon Gold 5115" "Intel Xeon Gold 5117" "Intel Xeon Gold 5118"
"Intel Xeon Gold 5120" "Intel Xeon Gold 5120T" "Intel Xeon Gold 5215" "Intel Xeon Gold 5217" "Intel Xeon Gold 5218"
"Intel Xeon Gold 5218R" "Intel Xeon Gold 5218T" "Intel Xeon Gold 5220" "Intel Xeon Gold 5220R" "Intel Xeon Gold 5222"
"Intel Xeon Gold 5315Y" "Intel Xeon Gold 5317" "Intel Xeon Gold 5318Y" "Intel Xeon Gold 5320" "Intel Xeon Gold 6122"
"Intel Xeon Gold 6126" "Intel Xeon Gold 6128" "Intel Xeon Gold 6130" "Intel Xeon Gold 6130T" "Intel Xeon Gold 6132"
"Intel Xeon Gold 6134" "Intel Xeon Gold 6136" "Intel Xeon Gold 6137" "Intel Xeon Gold 6138" "Intel Xeon Gold 6138T"
"Intel Xeon Gold 6140" "Intel Xeon Gold 6143" "Intel Xeon Gold 6144" "Intel Xeon Gold 6146" "Intel Xeon Gold 6148"
"Intel Xeon Gold 6150" "Intel Xeon Gold 6152" "Intel Xeon Gold 6154" "Intel Xeon Gold 6208U" "Intel Xeon Gold 6210U"
"Intel Xeon Gold 6212U" "Intel Xeon Gold 6226" "Intel Xeon Gold 6226R" "Intel Xeon Gold 6230" "Intel Xeon Gold 6230R"
"Intel Xeon Gold 6238" "Intel Xeon Gold 6238R" "Intel Xeon Gold 6242" "Intel Xeon Gold 6242R" "Intel Xeon Gold 6244"
"Intel Xeon Gold 6246" "Intel Xeon Gold 6246R" "Intel Xeon Gold 6248" "Intel Xeon Gold 6248R" "Intel Xeon Gold 6250"
"Intel Xeon Gold 6252" "Intel Xeon Gold 6253CL" "Intel Xeon Gold 6254" "Intel Xeon Gold 6278C" "Intel Xeon Gold 6312U"
"Intel Xeon Gold 6314U" "Intel Xeon Gold 6326" "Intel Xeon Gold 6330" "Intel Xeon Gold 6334" "Intel Xeon Gold 6336Y"
"Intel Xeon Gold 6338N" "Intel Xeon Gold 6342" "Intel Xeon Gold 6346" "Intel Xeon Gold 6348" "Intel Xeon Gold 6354"
"Intel Xeon Platinum 8124M" "Intel Xeon Platinum 8160" "Intel Xeon Platinum 8167M" "Intel Xeon Platinum 8168"
"Intel Xeon Platinum 8171M" "Intel Xeon Platinum 8173M" "Intel Xeon Platinum 8175M" "Intel Xeon Platinum 8176"
"Intel Xeon Platinum 8180" "Intel Xeon Platinum 8259CL" "Intel Xeon Platinum 8260" "Intel Xeon Platinum 8260M"
"Intel Xeon Platinum 8268" "Intel Xeon Platinum 8275CL" "Intel Xeon Platinum 8280" "Intel Xeon Platinum 8347C"
"Intel Xeon Platinum 8358" "Intel Xeon Platinum 8375C" "Intel Xeon Platinum 8380" "Intel Xeon Platinum P-8124"
"Intel Xeon Silver 4109T" "Intel Xeon Silver 4110" "Intel Xeon Silver 4110" "Intel Xeon Silver 4114" "Intel Xeon Silver 4114"
"Intel Xeon Silver 4116" "Intel Xeon Silver 4116T" "Intel Xeon Silver 4123" "Intel Xeon Silver 4208" "Intel Xeon Silver 4209T"
"Intel Xeon Silver 4210" "Intel Xeon Silver 4210" "Intel Xeon Silver 4210R" "Intel Xeon Silver 4210R" "Intel Xeon Silver 4214"
"Intel Xeon Silver 4214R" "Intel Xeon Silver 4214Y" "Intel Xeon Silver 4215" "Intel Xeon Silver 4215R" "Intel Xeon Silver 4216"
"Intel Xeon Silver 4309Y" "Intel Xeon Silver 4310" "Intel Xeon Silver 4310T" "Intel Xeon Silver 4314" "Intel Xeon Silver 4316"
)
cpu_count=0
cpu_card=$(lscpu | grep -i "model name" | sed "s/Inter(R)/\Inter/g" | sed "s/Xeno(R)/\Xeno/g")
for cpucards in "${cpu_cards[@]}"; do
    if [echo "$cpu_card" | grep "$cpucards"];then
	    ((cpu_count++))
	fi
done
if ["$cpucount" -eq 0];then
	echo -e "\033[41mcpu：$cpu_card 不是常用cpu\033[0m"
else
	echo -e "\033[42mcpu：$cpu_card 是常用cpu\033[0m"
fi
    
#磁盘测速ssd>350mb/s | hdd > 100mb/s
hddfail=1
for hdd in "${hdd_names[@]}"; do
    speed=$(fio -name=machine_sn_sd --ioengine=libaio --direct=1 --group_reporting --runtime=128 --iodepth=32 --rw=randwrite --bs=4m --size=4G --numjobs=1 --thread --filename=$hdd --output-format=json | grep "bw" | awk 'NR==2 {print $3}')
    if ["$speed" -lt 100];then
	    echo -e "\033[41m$hdd 测速不合格\033[0m"
		((hddfaid++))
	fi
done
if ["$hddfail" -eq 1];then
    echo -e "\033[42mHDD测速合格\033[0m"
fi

#SSD测速
ssdfail=1
ssd_names=($(lsblk -d -n -p -o NAME,ROTA | awk '$2==0 {print $1}'))
for ssd in "${ssd_names[@]}"; do
    speed=$(fio -name=machine_sn_sd --ioengine=libaio --direct=1 --group_reporting --runtime=128 --iodepth=32 --rw=randwrite --bs=4m --size=4G --numjobs=1 --thread --filename=$ssd --output-format=json | grep "bw" | awk 'NR==2 {print $3}')
    if ["$speed" -lt 350];then
	    echo -e "\033[41m$ssd 测速不合格\033[0m"
		((sddfaid++))
	fi
done
if ["$ssdfail" -eq 1];then
    echo -e "\033[42mSDD测速合格\033[0m"
fi













